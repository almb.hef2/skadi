
![](skadi_print_processes.png) ![](skadi_print_temperature.png)

Skadi é um aplicativo para monitoramento de temperatura para sistemas Android.

Além disso, ele também permite o monitoramento de processos e fornece a opção de encerrar aqueles que mais consomem processamento e, consequentemente, aquecem mais o aparelho. 
    
Esse projeto é resultado do HandsOn do curso de capacitação em desenvolvimento embarcado do projeto HEFESTO.
