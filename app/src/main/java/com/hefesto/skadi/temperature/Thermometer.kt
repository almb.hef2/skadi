package com.hefesto.skadi.temperature

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.BatteryManager
import android.os.PowerManager
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

class Thermometer {

    companion object {

        fun getBatteryTemperature(context: Context): Int {


            val intent_filter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            val battery_status = context.registerReceiver(null, intent_filter)
            val temp_int: Int =
                (battery_status?.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) ?: 0) / 10
            return temp_int
        }

        fun getThermalStatus(context: Context): String {
            var PM = context.getSystemService(Context.POWER_SERVICE) as PowerManager
            return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                when (PM.currentThermalStatus) {
                    0 -> "Neutro"
                    1 -> "Luz"
                    2 -> "Moderado"
                    3 -> "Grave"
                    4 -> "Crítico"
                    5 -> "Emergência"
                    6 -> "Desligamento"
                    else -> throw IllegalArgumentException("Erro de leitura da temperatura: $this")
                }
            } else {
                "Status não disponível."
            }
        }

        fun getCPUTemperature(): Int {
            var current_temp : Int = -31
            for (i in 0 until temp_paths.size)
            {
                var temp = readOneLine(File(temp_paths[i]))
                if (temp != null) {
                    if (isTemperatureValid(temp)) {
                        current_temp = temp
                    } else if (isTemperatureValid(temp / 1000)) {
                        temp = temp / 1000
                        current_temp = temp
                    }
                    break
                }
            }
            return current_temp
        }

        private fun isTemperatureValid(temp: Int): Boolean = temp in -30..250

        fun getCPUThermalStatus(temp : Int) : String {
            if (temp in -30..60)
                return "Normal"
            else if (temp in 60..80)
                return "Warm"
            else
                return "Critical"
        }

        fun getBatteryThermalStatus(temp : Int) : String {
            if (temp in -30..40)
                return "Normal"
            else if (temp in 40..60)
                return "Warm"
            else
                return "Critical"
        }

        fun getThermalStatusColor(status : String): Int {
            return when (status) {
                "Normal" -> Color.GREEN
                "Warm" -> Color.YELLOW
                "Critical" -> Color.RED
                else -> Color.WHITE
            }
        }

        private val temp_paths = listOf(
            "/sys/devices/system/cpu/cpu0/cpufreq/cpu_temp",
            "/sys/devices/system/cpu/cpu0/cpufreq/FakeShmoo_cpu_temp",
            "/sys/class/thermal/thermal_zone0/temp",
            "/sys/class/i2c-adapter/i2c-4/4-004c/temperature",
            "/sys/devices/platform/tegra-i2c.3/i2c-4/4-004c/temperature",
            "/sys/devices/platform/omap/omap_temp_sensor.0/temperature",
            "/sys/devices/platform/tegra_tmon/temp1_input",
            "/sys/kernel/debug/tegra_thermal/temp_tj",
            "/sys/devices/platform/s5p-tmu/temperature",
            "/sys/class/thermal/thermal_zone1/temp",
            "/sys/class/hwmon/hwmon0/device/temp1_input",
            "/sys/devices/virtual/thermal/thermal_zone1/temp",
            "/sys/devices/virtual/thermal/thermal_zone0/temp",
            "/sys/class/thermal/thermal_zone3/temp",
            "/sys/class/thermal/thermal_zone4/temp",
            "/sys/class/hwmon/hwmonX/temp1_input",
            "/sys/devices/platform/s5p-tmu/curr_temp"
        )

        /**
         * Read and parse to Long first line from file
         */
        fun readOneLine(file: File): Int? {
            val text: String?
            try {
                val fs = FileInputStream(file)
                val sr = InputStreamReader(fs)
                val br = BufferedReader(sr)
                text = br.readLine()
                br.close()
                sr.close()
                fs.close()
            } catch (ex: Exception) {
                return null
            }

            val value: Int?
            try {
                value = text.toInt()
            } catch (nfe: NumberFormatException) {
                return null
            }
            return value
        }

    }

}