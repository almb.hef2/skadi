package com.hefesto.skadi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.skadi.process.ProcessesFragment
import com.example.skadi.temperature.TemperatureFragment
import com.google.android.material.bottomnavigation.BottomNavigationMenu
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemReselectedListener,
    BottomNavigationView.OnNavigationItemSelectedListener {

    val temp_frag : Fragment = TemperatureFragment.newInstance("temperature", "fragment")
    val process_frag : Fragment = ProcessesFragment.newInstance("process", "fragment")
    val fm : FragmentManager = supportFragmentManager
    var active : Fragment = temp_frag

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fm.beginTransaction().add(R.id.fragment_container, process_frag, "2").hide(process_frag).commit()
        fm.beginTransaction().add(R.id.fragment_container, temp_frag, "1").commit()

        var navigation_view = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        navigation_view.setOnNavigationItemSelectedListener(this)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId)
        {
            R.id.temperature -> {
                fm.beginTransaction().hide(active).show(temp_frag).commit()
                active = temp_frag
                return true
            }
            R.id.process -> {
                fm.beginTransaction().hide(active).show(process_frag).commit()
                active = process_frag
                return true
            }
        }
        return false
    }

    override fun onNavigationItemReselected(item: MenuItem) {
//        when (item.itemId)
//        {
//            R.id.temperature -> {
//                fm.beginTransaction().hide(active).show(temp_frag).commit()
//                active = temp_frag
//            }
//            R.id.process -> {
//                fm.beginTransaction().hide(active).show(process_frag).commit()
//                active = process_frag
//            }
//        }
    }

}